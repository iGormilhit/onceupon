---
title:
- type: main
  text: Once Upon ...
- type: subtitle
  text: imaginations-02-bib
author: iGor
bibliography: /home/igor/zotero.json
csl: /home/igor/styles/iso690-author-date-fr.csl
locale: fr-FR

---

# L'étagère de onceupon

Elle se constitue sur Zotero : [onceupon](https://www.zotero.org/igor.m/items/collectionKey/3WTNFGN4 "collection 'onceupon' sur Zotero").

On pourrait imaginer aussi qu'elle dispose d'un ouvrage bibliographique justement, divers catalogues, par exemple celui des livres inexistants, mais aussi d'autres ouvrages.

ROBERT, Paul et REY, Alain, 2001. _Le grand Robert de la langue française : dictionnaire alphabétique et analogique de la langue française_. 2e éd. Paris : Le Robert. ISBN 2850366730. 


