---
title:
- type: main
  text: Once Upon ...
- type: subtitle
  text: imaginations-01
author: iGor
bibliography: /home/igor/zotero.json
csl: /home/igor/styles/iso690-author-date-fr.csl
locale: fr-FR

---

# Imagination 01

## Source

Comme souvent, pour mieux m'endormir, j'imagine une situation dans laquelle je peux m'évader et m'y sentir bien. Ça tourne souvent autour des mêmes dispositifs. En l'occurrence, c'était en novembre, en période de pluie, avec la sensation de froid automnal s'annonçant, même si la température était assez clémente en cette fin d'année 2014.

J'ai imaginé une petite cabane de montagne, comme la cabane de chasseur que l'on peut visiter sur l'ancien bisse de Saxon. Elle serait organisée un peu différemment, peut-être avec une seule pièce. Une partie des murs pourraient être en pierre, ce qui suppose les capacités socio-techniques pour le faire.

La particularité de la situation, ce qui m'intéresse entre autres d'explorer, est le rapport entre la présence, dans la cabane, d'une petite étagère avec quelques documents imprimés : dictionnaires (par exemple la dernière édition du Grand Robert, 2001), quelques romans, essais, en réalité une source d'information limitée, et signalant l'ancienne existence de sources différentes, presque sans borne : Internet et le Web. Or, cette femme, elle, se souvient du monde qui utilisait encore ces technologies désormais disparues (ou presque).

## 1^er^ développement

Dans l'histoire que j'essaie de construire à partir de cette petite situation imaginée, la cabane est habitée par une femme seule. Elle est passablement occupée à simplement vivre, ce qui suppose se nourrir, chauffer le local, etc. Il serait peut-être nécessaire d'imaginer également une activité permettant de faire un peu de commerce, ou de rendre un service, afin d'obtenir de la collectivité humaine des choses qu'elle ne peut obtenir ou réaliser seule.

Selon les saisons, selon les rythmes, il reste des périodes moins denses en activité, des périodes où la lumière du jour est moins présente --- ce qui pose la question de l'éclairage : la lecture à la lumière de la bougie, ou de lampe à huile, c'est pas idéal ---, où il reste du temps pour lire, penser, chercher, voire écrire.

## Contexte

Elle habite dans cette cabane qui est passablement éloignée de toute autre habitation humaine, du moins connue par elle. Il lui faut plusieurs jours de marche pour atteindre la première agglomération : village ?, ville ? Quoi qu'il en soit, elle doit descendre en plaine, peut-être traverser plusieurs vallées inhabitées, ou très peu. Car le monde est bien moins habité dans ce futur proche que dans le premier quart du 21^e^ siècle. On peut imaginer que la population mondiale soit réduite à 2 ou 3 milliards d'habitants. Surtout, les structures socio-politiques connues actuellement n'existent plus. Et les infrastructures de communication, la production industrielle et la logistique de transports sont extrêmement réduites.

Au niveau politique, il y a quelques potentats privés, anciennes traces des inégalités de richesses connues au 21^e^ siècle. Mais pour le reste de l'organisation politique, les choses sont assez diverses, relativement vagues : des formes de démocraties horizontales, qui discutent et règlent leurs problèmes de manière horizontale entre elles également. On trouve également des formes d'organisation plus proches de nos républiques vaguement démocratiques. Les nations n'existent (presque) plus. Mais ces points n'ont pas à apparaître de manière explicite, mais indirectement, comme si tout cela va un peu de soit. Pour autant, ce serait quand même intéressant qu'on puisse se faire une idée des processus de consensus tels que décrits par @graeber_comme_2014.

## Histoire

* **Entre mi et fin novembre.** Période de préparation pour l'hiver. Momentanément, il y a quelques jours de pluie, où elle ne va pas pouvoir sortir beaucoup, mais elle a pu engranger quelques réserves. Elle peut donc prendre un peu de repos, ou de temps pour lire, mais en l'occurrence ça revient principalement à se documenter. Elle a notamment besoin de savoir certaines choses sur l'agriculture de montagne, peut-être plus précisément identifier une maladie, et apprendre comment la combattre. Elle pourrait tomber sur quelque chose qu'elle ne parvient pas à comprendre. Elle essaie de résoudre l'incompréhension avec la documentation dont elle dispose, mais n'y parvient pas. Elle hésite à "descendre en ville" pour éventuellement consulter d'autres sources. Mais, d'une part "la ville" est à plusieurs jours de marche et d'autre part, l'hiver menace : doit-elle prendre le risque que la neige tombe pendant son absence et qu'il soit difficile de revenir de la ville (passage de cols) ?   
Ou à l'inverse, la venue de la neige permettrait à la fois de se déplacer en ski et de pouvoir partir, puisque très peu de travail la retiendrait au refuge. Elle pourrait décider d'attendre la venue de la neige. Ce qui permet de décrire le contenu de sa bibliothèque, et l'*Histoire* qu'elle connaît. Elle passe donc son temps à se balader dans sa bibliothèque, à réfléchir à l'évolution des choses et à préparer son voyage, ce qui n'est pas très simple, parce que vu son âge, elle ne peut pas se permettre de porter beaucoup de poids (peut-être un traîneau ?), mais elle a tout de même besoin de pouvoir survivre 3-4 jours en déplacement, avec le risque que ça se passe mal.
* **Entre mi et fin décembre**, arrive le grand froid, puis la neige : elle peut partir en ski. D'abord il y a la descente vers la pleine. Puis il faut remonter la vallée, en direction du Nord-Ouest, pendant quelques heures et, enfin, monter vers le cols. Tôt dans la montée, elle trouve une toute petite cabane, dans laquelle il y a un poêle, un peu de bois, un banc et une simple couche. Elle peut y passer la nuit. Le lendemain, montée au col, attention aux avalanches, passage du col, descente dans la nouvelle vallée qui est orientée de manière opposée à la première (elle descent vers le Nord-Ouest). Cette dernière débouche sur une plaine, et il faut encore deux jours de marche avant d'arriver à la ville. Là, elle ne trouve pas de refuge. Par contre, la dernière nuit, elle l'a passe en compagnie d'un autre voyageur, qui revient "des bois".
