> "[dans les textes de fiction] il se dit quelque chose que les systèmes dominants mettent entre parenthèses et ne peuvent donc intégrer au monde tel qu'ils le socialisent. La fiction constitue la cohérence globale de la réalité : elle ne s’oppose pas à elle, elle la communique.^[Wolfgang Iser, _Au sujet du cimetière marin_, Pléiade, &OElig;uvres T.1, p. 14 ? 1497; sources : [1](http://semen.revues.org/5383) et [2](http://insaniyat.revues.org/7355))]"

# Once Upon ...

Projet d'écriture. Petite cabane en montagne, bibliothèque réduite, très réduite, avec le souvenir de ce qui avait existé, Internet et le Web. Peut éventuellement s'ajouter d'autres élément, comme une aventure, voire une aventure d'ordre spatiale.

## Sessions

Le répertoire `sessions/` contient les sessions d'écritures proprement dites. Pour l'instant il n'est pas décidé comment elles seront organisées entre elles. Il est vraisemblables qu'elles ne s'organisent pas par temporalité, mais par thématiques, ou par "scènes", chapitres, parties.

## Imaginations

Le répertoire `imaginations/` comprend des fichiers "imaginant" ce que sera l'histoire. Je pense que j'y intègrerai également les éléments de documentation. Celle-ci n'est pas à proprement parlé de l'imagination, mais elle donne de la chair à ce qui est imaginé. Et c'est déjà bien assez de répertoires comme ça.

# Compilation

Pour compiler et obtenir les fichiers EPUB et PDF, il faut :

1. un environnement UNIX
1. une version de pandoc à jour
1. une version de pandoc-citeproc à jour
1. lancer `make all` ou `make pdf` ou `make epub`.

