---
title:
- type: main
  text: Once Upon ...
- type: subtitle
  text: session-04
  author: iGor
bibliography: /home/igor/zotero.json
csl: /home/igor/styles/iso690-author-date-fr.csl
locale: fr-FR

---

C'était une étrange époque, où il y avait une exigence morale d'identifier qui avait prétendument été l'énonciateur de telle ou telle suite de phrases, de telle ou telle idée, de tel ou tel raisonnement. Et cette exigence morale était fondée sur l'unique motivation du profit, comme une évidence impossible à questionner, voire à remettre en cause. Parce que sans profit, chacun était réduit au salaire, pourtant la norme, mais une norme dont chacun avait honte. La dignité prenait sa source uniquement dans la capacité à vivre sans salaire. Rentes sur un capital inépuisable, si possible hérité. Ou le profit.

Pourtant, quelle est l'importance de qui a dit quoi. J'ai entendu récemment les mots suivants, ou à peu près : « On ne peut comprendre la vie qu'*a posteriori*, mais on ne peut vivre qu'en mouvement vers demain. » À peu près. En fait pas du tout. L'original était dans une langue scandinave, puis elle a été traduite en anglais, sortie de son contexte, insérée dans un autre contexte, et maintenant je la traduis, librement, en français, et je modifie complètement le contexte.

C'est le processus de l'adaptation de la vie. Ce n'est pas sans risque, de telles modifications peuvent... Mais comment différencier évolution et cancer ?
