---
title:
- type: main
  text: Once Upon ...
- type: subtitle
  text: session-02
  author: iGor
bibliography: /home/igor/zotero.json
csl: /home/igor/styles/iso690-author-date-fr.csl
locale: fr-FR

---

# Session 2 (récit cadre)

Pendant de longs mois, je me suis endormi en imaginant les mêmes bribes de la même histoire, les mêmes bribes du même univers. C'est quelque part dans l'avenir, cet avenir post-apocalyptique qui est presque une certitude, voire une promesse. Comme la Terre, la Californie ou l'au-delà : l'*El Do-delà*. C'est quelque part dans une zone montagneuses, de grandes vallées, profondes, vides d'animaux bipèdes. Sur le coteau d'une de ces vallées, on trouve un petit vallon, au sortir de la forêt, et sur la crête qui sépare le vallon de la dérupe dans la vallée, un refuge, flanqué d'une grange. Et dans le refuge, une femme, seule. Dans la grange, du foin en bonnes quantités, et une mule, quelques chèvres et moutons.

Dans un premier temps, je fais le tour, par bribes de bribes, la juxtaposition d'atomes de mondes, sans m'attarder sur les liaisons. Elles vont de soi, et peuvent se permettre de manquer à l'appel. D'ailleurs, personne ne les appelle.   
Parce que c'est l'hiver, les bêtes sont rentrées dans l'étable ou dans l'écurie, je ne sais plus trop bien, si j'ai jamais su, mais qu'est-ce que ça change ? Étonnamment l'intérieur de la *grange-étable-écurie* est très lumineux. Il est possible de traverser cet espace entre l'extérieur et l'intérieur de la cabane refuge, avec d'un côté une grande porte à deux battants et de l'autre une simple porte par laquelle on entre dans le petit espace réservé aux humains, en l'occurrence à la seule occupante des lieux.   
Il arrive que la disposition des choses à l'intérieur du *refuge-cabane* évolue, mais ça ne doit pas empêcher les mots de les fixer provisoirement. Si l'on rentre depuis la grange, à sa gauche se trouve un espace dédié à la &laquo;&nbsp;cuisine&nbsp;&raquo;, à savoir un poêle en pierre et une sorte de lavabo avec une arrivée d'eau. Celle-ci pourrait aisément être qualifiée de miraculeuse, bien qu'elle existe sans éprouver la nécessité de se justifier. Bien sûr, pour montrer quelque respect à la vraisemblance, le robinet ne propose aucune eau chaude.
Toujours sur la gauche, après la &laquo;&nbsp;cuisine&nbsp;&raquo;, une porte donne vers l'extérieur, en direction du petit vallon, à l'opposé de la pente vers la vallée. Le fond de la pièce, juste après la porte, est occupé par un lit, parfois double et perpendiculaire au mur, parfois simple et parallèle, mais surmonté d'une deuxième banquette. Enfin, en terminant le tour de la pièce de gauche à droite, en face de la &laquo;&nbsp;cuisine&nbsp;&raquo;, une table, deux tabourets et un banc contre le mur. Contre l'autre mur, dans le sens de la largeur, une étagère supporte quelques livres. La pièce dispose de deux fenêtres, l'une au-dessus du lavabo, la deuxième derrière la table. De même, il y a deux lampes à huile.
