---
title:
- type: main
  text: Once Upon ...
- type: subtitle
  text: session-01b
  author: iGor
bibliography: /home/igor/zotero.json
csl: /home/igor/styles/iso690-author-date-fr.csl
locale: fr-FR

---

# Session 01

Le mauvais temps avait menacé tout au long de la journée. D'abord une bise glacée, chargée de nuages de plus en plus sombres. Puis, le vent s'était calmé et un ciel noir encombrait toute la vallée. Enfin, alors qu'elle sortait de la forêt avant de traverser la petite combe qui menait au refuge, la pluie s'était mise à tomber. Elle s'était annoncée et se contentait de bruiner, comme si elle voulait laisser au monde le temps de l'apprivoiser. Elle arrivait au bon moment, comme bienvenue. La réserve était raisonnablement remplie. De quoi manger, de quoi nourrir le poêle. Il ne suffirait plus que d'aller chercher de temps à autre de l'eau. Sous la pluie. Elle ressentait la fatigue et la satisfaction d'avoir bien travaillé. La pluie venait alors qu'elle était entièrement disposée à l'accueillir, ce qui était plutôt rare.

La pluie était bien venue, à l'heure, comme si elle avait pris rendez-vous. Elle l'avait attendue en se préparant. Et voici qu'elle était arrivée, la pluie de novembre. Elle diluait les rouges, les jaunes, les bruns, tirait tout ça vers le gris. Surtout, elle faisait chanter la nuit, une berceuse à une corde, monotone, régulière. Elle donnait confiance, la pluie, alors que pétait et sifflait le bois dans le poêle. Une tisane, une bougie, un dictionnaire. Un grand dictionnaire, en neuf volumes^[[@robert_grand_2001]].

Un dictionnaire. C'était un objet curieux, un inventaire des mots, avec leur description, leur définition. À sa connaissance, personne ne faisait plus de dictionnaires aujourd'hui. Peut-être qu'en consultant le _Grand Robert_ justement, elle aurait appris l'existence d'un verbe qui voulait dire "faire un dictionnaire". Un verbe qu'elle aurait peut-être pu connaître, mais qu'elle aurait de toute manière oublié : plus personne ne faisait de dictionnaire. Et d'ailleurs, ce dictionnaire avait été imprimé _avant_. On y trouvait beaucoup de mots qui n'étaient plus utilisés, ou alors d'une façon qu'on ne comprenait presque plus. Et il y manquait bien d'autres. Surtout, il lui manquait la sensibilité aux nuances géographiques. Le même mot, de quelques jours de marche en quelques jours de marche, ne signifiait pas exactement la même chose. Ce dictionnaire était vraiment un objet curieux.
