---
title:
- type: main
  text: Once Upon ...
- type: subtitle
  text: session-03
  author: iGor
bibliography: /home/igor/zotero.json
csl: /home/igor/styles/iso690-author-date-fr.csl
locale: fr-FR

---

# Session 3 (journal)

## [entrée de calendrier]^[ce qui suppose en définir un]

Dans quelques minutes, je vais partir comme prévu pour explorer le bunker. Tout est prêt. Les affaire pour le voyage. J'ai prévenu les Müller, si dans une semaine je ne suis pas de retour, ils viendront gouverner le cheval^[trouver un nom pour le cheval, ou l'âne peut-être] et les moutons. Par contre, je prends le chien^[et hop, un nom de plus à trouver]. Et donc, de quoi manger pour lui.   
Si dans deux semaines ils n'ont pas de nouvelles, alors ils devront décider eux-mêmes que faire. Je leur ai dit que je n'attendais pas qu'ils essaient de me retrouver, même que je leur déconseillais de le tenter. C'est pour la ferme qu'ils devront décider : laisser tomber, en retirer ce qui leur est utile, ou la reprendre pour eux. Bientôt, ils seront assez nombreux pour vivre sur deux fermes. Si ça devait arriver, je me demande ce qu'en penseraient les Shefqet. Mais ils sont quand même bien loin.

Mais je serai de retour dans la semaine. J'ai décidé d'être prudente, comme à mon habitude. Ce n'est pas de la curiosité mal placée. Il faut qu'on sache ce qui est si près de nous, peut-être sous nos champs, sous nos pieds. Sous nos fermes.

## [entrée de calendrier, 6 jours après l'entrée précédente]

Je viens d'avertir les Müller, Peter a eu l'air soulagé de me savoir saine et sauf. Il était curieux, forcément, mais je ne lui ai rien dit, on en parlera de vive voix. La première chose que j'ai fait en arrivant a été d'aller voir les bêtes. Elles vont bien, elles étaient contentes que je m'en occupe, c'est quand même très limite de les laisser comme ça si longtemps. Si je repars en exploration, il faudra trouver une autre solution. Seule dans cette ferme, ce n'est pas très raisonnable.   
Mais pour l'instant, je vais me coucher, je n'en peux plus.

## [entrée de calendrier, 2 jours après la précédente]

Il faudra bien que je raconte l'expédition, parce que je n'ai pas fini d'en explorer les conséquences. Je suis donc partie au petit matin du [8<sup>e</sup> jour précédent], un ciel bien découvert, une belle lune, ça permettait de marcher malgré la nuit. Je me suis dirigée vers les pâturages d'été, puis vers le « Col des petits lacs ». L'entrée du bunker est à deux heures de marche sur la gauche. Il faut quitter le chemin un peu après les « Ruines » et remonter. L'hiver on ne voit pas l'entrée de loin, mais elle se situe au fond de la falaise de la « Tour du Mont noir ». Il m'a quand même fallu la chercher un bon moment et c'est le chien qui l'a trouvée. Il l'a fait l'air de rien, comme si on jouait à creuser dans la neige. Mais je ne jouait pas vraiment, il commençait à faire tard, et je ne me voyais pas bivouaquer à la belle étoile à cette époque. L'intérieur n'était pas beaucoup plus confortable, mais au moins, c'était à peu près sec et j'ai pu trouver le moyen de m'abriter au mieux des courants d'air.

La nuit a été éprouvante, à cause du froid. On s'est glissé ensemble sous les couvertures, [le chien] et moi, et du coup ça allait un peu mieux, mais ça restait plutôt glacial. Bien entendu, je n'ai rien trouvé pour faire du feu. Il aurait fallu marcher quelques heures de plus pour aller chercher du bois plus bas en altitude. Sur le moment, je me suis dit que j'aurais dû en prendre avec moi, mais ça n'aurait pas été raisonnable, j'étais déjà bien chargée.

Sans surprise, je me suis levée avant le soleil. Je suis sortie l'observer. C'était très beau. Une émotion difficile à comprendre, ou à décrire. Comme si je pouvais observer un monde dans lequel il n'y a plus personne, pas même moi. Et pourtant, peu à peu, la lumière du soleil me réchauffait. Au moins un peu. J'étais impatiente d'aller explorer le bunker, mais j'avais aussi de la peine à me résoudre à quitter la chaleur, relative, du soleil.

Alors, j'ai commencé mon exploration. J'ai laissé une partie de mon chargement là où j'avais dormi et je suis entrée plus en avant dans le bunker. Au fond de la pièce qui était accessible directement depuis l'extérieur, il y avait une partie passablement effondrée. Je n'ai pas trouvé d'autre moyen pour pénétrer la montagne, alors j'ai commencé à désescalader ce trou encombré de morceaux de béton, en espérant que ça m'amène quelque part. La progression était assez difficile, surtout avec [le chien] qui a eu plusieurs fois besoin de mon aide pour descendre.

Au fur et à mesure de la descente^[Problème de la lumière], la température s'est stabilisée, même si ça restait froid, et surtout passablement humide. On est descendu ainsi pendant un peu plus de deux heures et soudain la structure des lieux a commencé à ressembler à quelque chose. J'ai pu commencer à explorer les lieux de manière un peu systématique. Assez vite, je me suis mise à tracer des signes à certains embranchements avec un bout de béton, dans l'espoir de retrouver mon chemin sans trop de difficulté. Dans l'ensemble, les lieux sont vides. Des couloirs, quelques pièces, dont certaines ont des ouvertures dans la paroi de la montagne et qui offrent une vue saisissante sur la vallée de chez les [?]^[qui?]. Parfois, il reste quelques structures en béton qui devaient accueillir des éléments qui ont disparus.   
Au bout de quelques heures, j'ai eu le sentiment de me trouver dans une impasse. Je ne comprenais pas comment continuer. Face au mur. Mais j'étais convaincue que ce n'était pas tout, qu'il y avait autre chose. Je me suis arrêtée, autant pour faire une pause, manger un peu, que pour réfléchir.

En réalité, les espaces que j'avais parcourus n'étaient pas si complexes, je pouvais m'en faire une image assez précise. Mes petites traces n'allaient pas m'être nécessaires désormais, mais elles m'ont aidé à me représenter l'organisation des lieux. Et celle-ci ne suffisait pas à donner du sens à ce bunker, il devait y avoir autre chose. Qui devait nécessairement se situer de l'autre côté du mur contre lequel je m'était appuyée, assise. Une fois ma pause terminée, je me suis remise à chercher, n'importe quoi, quelque chose, un signe qui pourrait me faire comprendre comment continuer. Mais rien. Je ne voyais rien. Et je n'arrivais pas à me résoudre à abandonner, à quitter les lieux. Le manque de lumière n'aidait pas à m'assurer de ne rien louper. Je sentais aussi que le chien devenait nerveux. Et toujours rien. J'ai fini par décider de passer la nuit, ou du moins ce qui me semblait ressembler à une nuit dans ce sous-sol sans lumière et de voir au réveil que faire.

Le chien m'a réveillé au milieu de la nuit en aboyant. Ce réveil brusque, dans une totale obscurité, me laissait passablement désorientée. J'ai mis un moment à comprendre où j'étais. Mais pourquoi le chien aboyait ? Depuis combien de temps ça durait ? J'ai essayé de le calmer, d'abord sans succès et puis peu à peu il s'est presque tu. Et c'est là que j'ai entendu quelqu'un me parler.
