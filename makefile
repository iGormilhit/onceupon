# Makefile pour la production du ePub et du PDF
# Sur la base d'un makefile de @jbfavre :
# https://github.com/jbfavre/JudAllanBook/blob/master/utilities/makefile
#
# version 0.0.2

SHELL := /bin/bash

PANDOC := $(shell pandoc --version 2>/dev/null)
pandoc_bin=/usr/bin/pandoc

make_dir=$(shell pwd)
epub_dir=$(make_dir)/epub
pdf_dir=$(make_dir)/pdf

%.check:
	@echo -n "Searching pandoc... "
ifdef PANDOC
	@which pandoc
else
	@echo "NOT FOUND"
	@exit 1
endif
	@echo "Preparing build environment... "
	@mkdir -p $(epub_dir)
	@mkdir -p $(pdf_dir)
	@rm -rf $(epub_dir)/*
	@rm -rf $(pdf_dir)/*

%-epub:
	@echo "Generating epub... "
	@$(pandoc_bin) \
		-t epub3 \
		--css=blitz.css \
		--toc --toc-depth=2 \
		--epub-chapter=1 \
		--filter pandoc-citeproc \
		-o $(epub_dir)/onceupon.epub \
		title.txt \
		imaginations/*.md \
		sessions/*.md \
		bibliographie.md

%-pdf:
	@echo "Generating pdf... "
	@$(pandoc_bin) \
		--standalone \
		-t latex \
		--variable=latex-engine:xelatex \
		--variable=lang:fr \
		--variable=papersize:a4paper \
		--toc --toc-depth=2 \
		--top-level-division=chapter \
		--filter pandoc-citeproc \
		-o $(pdf_dir)/onceupon.pdf \
		title-pdf.txt \
		imaginations/*.md \
		sessions/*.md \
		bibliographie.md

%.all: | %-epub %-pdf
	@true

%: | %.check %.all
	@true
